package com.enderzombi102.progressbar2.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.enderzombi102.progressbar2.ProgressBar2;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.resizable = false;
		config.title = "ProgressBar 2";
		config.allowSoftwareMode = true;
		new LwjglApplication( new ProgressBar2(), config );
	}
}
