package com.enderzombi102.progressbar2;

import com.enderzombi102.progressbar2.behavior.Behavior;
import com.enderzombi102.progressbar2.unit.UnitType;
import com.google.common.collect.HashBiMap;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Random;

public final class Registry<KeyType, ValueType> {
	public static final Registry<String, Behavior> BEHAVIORS = new Registry<>();
	public static final Registry<String, UnitType> UNIT_TYPES = new Registry<>();
	private static final Object LOCKER = new Object();

	private final HashBiMap<KeyType, ValueType> objects = HashBiMap.create();
	private final ArrayList<ValueType> int2obj = new ArrayList<>();

	public @Nullable ValueType get( KeyType key ) {
		return objects.get( key );
	}

	public ValueType get( KeyType key, ValueType defaultValue ) {
		return objects.getOrDefault( key, defaultValue );
	}

	public @Nullable ValueType get( int rawKey ) {
		return rawKey < int2obj.size() ? int2obj.get( rawKey ) : null;
	}

	public int getRawKey( ValueType value ) {
		return int2obj.indexOf( value );
	}

	public ValueType register( KeyType key, ValueType value ) {
		synchronized ( LOCKER ) {
			int2obj.add(value);
			return objects.put(key, value);
		}
	}

	public ValueType getRandom( Random rnd ) {
		return int2obj.get( rnd.nextInt( int2obj.size() ) );
	}

	public KeyType getKey( ValueType value ) {
		return objects.inverse().get( value );
	}

	public int getSize() {
		return int2obj.size();
	}

	public static <ValueType, KeyType> ValueType register( Registry<KeyType, ValueType> registry, KeyType key, ValueType value ) {
		return registry.register( key, value );
	}
}
