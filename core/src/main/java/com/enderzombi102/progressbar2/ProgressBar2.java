package com.enderzombi102.progressbar2;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ScreenUtils;
import com.enderzombi102.progressbar2.unit.UnitTypes;

public class ProgressBar2 extends ApplicationAdapter {
	private Texture pieceTexture;
	private Texture playerTexture;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Rectangle player;

	@Override
	public void create() {
		// load units
		UnitTypes.ensureLoaded();

		System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
		Gdx.graphics.setWindowedMode(
				Gdx.graphics.getDisplayMode().width,
				Gdx.graphics.getDisplayMode().height
		);
		// load the images for the droplet and the bucket, 64x64 pixels each
		pieceTexture = new Texture( Gdx.files.internal("piece.png") );
		playerTexture = new Texture( Gdx.files.internal("player.png") );
		camera = new OrthographicCamera();
		camera.setToOrtho(
				false,
				Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()
		);
		batch = new SpriteBatch();
		player = new Rectangle();
		player.x = 800 / 2f - 64 / 2f;
		player.y = 20;
		player.width = 64;
		player.height = 64;
	}

	@Override
	public void render() {
		ScreenUtils.clear(0, 0, 0.2f, 1);
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(playerTexture, player.x, player.y);
		batch.end();

		if ( Gdx.input.isTouched() ) {
			Vector3 touchPos = new Vector3();
			touchPos.set( Gdx.input.getX(), Gdx.input.getY(), 0 );
			camera.unproject(touchPos);
			player.x = touchPos.x - 64 / 2f;
		}
		ScreenUtils.getFrameBufferTexture().getRegionWidth()
		if ( player.x < 0 )
			player.x = 0;
		if ( player.x + player.width > Gdx.graphics.getWidth() )
			player.x = Gdx.graphics.getWidth() - player.width;
	}
	
	@Override
	public void dispose() {
		batch.dispose();
		pieceTexture.dispose();
		playerTexture.dispose();
	}
}
