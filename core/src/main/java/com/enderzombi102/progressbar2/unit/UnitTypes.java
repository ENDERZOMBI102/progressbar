package com.enderzombi102.progressbar2.unit;

import blue.endless.jankson.api.SyntaxError;
import com.enderzombi102.progressbar2.Registry;
import com.enderzombi102.progressbar2.Util;

import java.io.IOException;

import static com.enderzombi102.progressbar2.Util.JANKSON;

public class UnitTypes {

	public static final UnitType NORMAL_UNIT;

	static {
		try {
			NORMAL_UNIT = Registry.register(
					Registry.UNIT_TYPES,
					"normal_unit",
					new UnitType(
							JANKSON.load( Util.getResourceInputStream( "" ) )
					)
			);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SyntaxError e) {
			e.printStackTrace();
		}
	}

	/**
	 * Ensure that all unit type objects are not null
	 */
	public static void ensureLoaded() {

	}
}
