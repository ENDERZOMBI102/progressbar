package com.enderzombi102.progressbar2.unit;

import blue.endless.jankson.JsonArray;
import blue.endless.jankson.JsonElement;
import blue.endless.jankson.JsonObject;
import blue.endless.jankson.JsonPrimitive;
import com.badlogic.gdx.graphics.Color;
import com.enderzombi102.progressbar2.behavior.Behavior;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class UnitType {
	protected final Color color;
	protected final String identifier;
	protected final List<String> supportedBehaviors = new ArrayList<>();

	protected UnitType( Color color, String identifier ) {
		this.identifier = identifier;
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public boolean isCompatible( Behavior behavior ) {
		return supportedBehaviors.contains( behavior.getIdentifier() );
	}

	public static @Nullable UnitType fromJson( JsonObject obj, String identifier ) {
		try {
			int color = obj.getInt("color", -1);
			if ( color == -1 )
				return null;
			var unit = new UnitType( new Color( color ), identifier );
			var behaviors = obj.get( JsonArray.class, "compatibleBehaviors" );
			assert behaviors != null;
			for (JsonElement behavior : behaviors) {
				unit.supportedBehaviors.add( ( (JsonPrimitive) behavior ).asString() );
			}
		} catch ( Exception e ) {

		}
	}
}
