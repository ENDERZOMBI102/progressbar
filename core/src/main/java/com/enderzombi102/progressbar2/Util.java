package com.enderzombi102.progressbar2;

import blue.endless.jankson.Jankson;

import java.io.InputStream;

public final class Util {

	public static final Jankson JANKSON = Jankson.builder().build();

	public static <T> T thisOrThat( T value0, T value1 ) {
		return value0 != null ? value0 : value1;
	}

	public static InputStream getResourceInputStream(String path) {
		return Util.class.getResourceAsStream(path);
	}
}
